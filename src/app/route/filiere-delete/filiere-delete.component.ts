import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';

import { Filiere } from 'src/app/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere-delete',
  templateUrl: './filiere-delete.component.html',
  styleUrls: ['./filiere-delete.component.css']
})
export class FiliereDeleteComponent implements OnInit {

  urlParam : string | null = null;
  filiere : Filiere = {libelle:''};

  constructor(private route : ActivatedRoute, private filiereService : FiliereService) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg');

    if (typeof(this.urlParam) === 'string'){
      const id = parseInt(this.urlParam);
      this.filiereService
        .getFiliere(id)
        .subscribe( (filiere : Filiere) => this.filiere = filiere)
      ;
    }
  }

  handleClick(){
    this.filiereService
        .deleteFiliere(this.filiere)
        .subscribe(data => console.log(data));
  }
}

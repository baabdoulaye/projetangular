import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereDeleteComponent } from './filiere-delete.component';

describe('FiliereDeleteComponent', () => {
  let component: FiliereDeleteComponent;
  let fixture: ComponentFixture<FiliereDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

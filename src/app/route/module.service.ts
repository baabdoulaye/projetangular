import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable} from 'rxjs';
import { switchMap, first} from 'rxjs/operators';
import { Module } from '../interfaces/module.interface';

const API: string = 'http://localhost:8080/api/module';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http: HttpClient) { }

  getModules(): Observable<Module[]>{
    return this.http.get<Module[]>(API);
  }

  getModule(id: number): Observable<Module>{
    return this.http.get<Module[]>(API)
    .pipe(
      switchMap(modules => modules),
      first( (module : Module) => module.id === id)
    );
  }

}

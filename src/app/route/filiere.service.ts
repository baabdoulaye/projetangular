import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';

import { Filiere } from '../interfaces/filiere.interface';

const API="http://localhost:8080/api/filiere";

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http : HttpClient) { }

  getFilieres() : Observable<Filiere[]>{
    return this.http.get<Filiere[]>(API);
  }

  getFiliere(id : number) : Observable<Filiere>{
    return this.http.get<Filiere[]>(API)
      .pipe(
        switchMap((filieres) => filieres),
        first( (filiere : Filiere) => filiere.id === id)
      );
    
  }

  deleteFiliere(filiere : Filiere){
    return this.http.put(API+"/"+filiere.id, null);
  }
}

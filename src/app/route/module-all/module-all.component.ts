import { Component, OnInit } from '@angular/core';
import { ModuleService } from '../module.service'
import { Module } from 'src/app/interfaces/module.interface';

@Component({
  selector: 'module-all',
  templateUrl: './module-all.component.html',
  styleUrls: ['./module-all.component.css']
})
export class ModuleAllComponent implements OnInit {

  modules: Module[] = [];

  constructor(private moduleService: ModuleService) {
    this.moduleService
    .getModules()
    .subscribe(modules => this.modules = modules)
  }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleAllComponent } from './module-all.component';

describe('ModuleAllComponent', () => {
  let component: ModuleAllComponent;
  let fixture: ComponentFixture<ModuleAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModuleAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

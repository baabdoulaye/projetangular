import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereCreateComponent } from './filiere-create.component';

describe('FiliereCreateComponent', () => {
  let component: FiliereCreateComponent;
  let fixture: ComponentFixture<FiliereCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'filiere-create',
  templateUrl: './filiere-create.component.html',
  styleUrls: ['./filiere-create.component.css']
})
export class FiliereCreateComponent implements OnInit {

  filiereCreateForm : FormGroup = new FormGroup({
    libelle : new FormControl('', Validators.required),
    modules : new FormArray([]),
    stagiaires : new FormArray([])
  });

  constructor() { }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{ HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { FiliereAllComponent } from './filiere-all/filiere-all.component';
import { FiliereByIdComponent } from './filiere-by-id/filiere-by-id.component';
import { FiliereCreateComponent } from './filiere-create/filiere-create.component';
import { FiliereUpdateComponent } from './filiere-update/filiere-update.component';
import { FiliereDeleteComponent } from './filiere-delete/filiere-delete.component';
import { ModuleAllComponent } from './module-all/module-all.component';
import { ModuleByIdComponent } from './module-by-id/module-by-id.component';
import { ModuleCreateComponent } from './module-create/module-create.component';
import { ModuleUpdateComponent } from './module-update/module-update.component';
import { ModuleDeleteComponent } from './module-delete/module-delete.component';
import { MenuComponent } from './menu/menu.component';



@NgModule({
  declarations: [
    FiliereAllComponent,
    FiliereByIdComponent,
    FiliereCreateComponent,
    FiliereUpdateComponent,
    FiliereDeleteComponent,
    ModuleAllComponent,
    ModuleByIdComponent,
    ModuleCreateComponent,
    ModuleUpdateComponent,
    ModuleDeleteComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'filiere', component: FiliereAllComponent},
      { path: 'filiere/create', component: FiliereCreateComponent},
      { path: 'filiere/update/:arg', component: FiliereUpdateComponent},
      { path: 'filiere/delete/:arg', component: FiliereDeleteComponent},
      { path: 'filiere/:arg', component: FiliereByIdComponent},
      { path: 'module', component: ModuleAllComponent},
      { path: 'module/:arg', component: FiliereByIdComponent}
    ])
  ],
  exports: [
    MenuComponent
  ]
})
export class RouteModule { }

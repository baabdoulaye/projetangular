import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Filiere } from 'src/app/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere-by-id',
  templateUrl: './filiere-by-id.component.html',
  styleUrls: ['./filiere-by-id.component.css']
})
export class FiliereByIdComponent implements OnInit {

  urlParam : string | null = null;
  filiere : Filiere | null = null;

  constructor(private route : ActivatedRoute, private filiereService : FiliereService) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg');

    if (typeof(this.urlParam) === 'string'){
      const id = parseInt(this.urlParam);
      this.filiereService
        .getFiliere(id)
        .subscribe( (filiere : Filiere) => this.filiere = filiere)
      ;
    }
  }
}

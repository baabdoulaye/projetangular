import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereByIdComponent } from './filiere-by-id.component';

describe('FiliereByIdComponent', () => {
  let component: FiliereByIdComponent;
  let fixture: ComponentFixture<FiliereByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

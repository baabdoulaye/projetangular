import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereAllComponent } from './filiere-all.component';

describe('FiliereAllComponent', () => {
  let component: FiliereAllComponent;
  let fixture: ComponentFixture<FiliereAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiliereAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/app/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere-all',
  templateUrl: './filiere-all.component.html',
  styleUrls: ['./filiere-all.component.css']
})
export class FiliereAllComponent implements OnInit {

  filieres : Filiere[] = [];

  constructor(private filiereService : FiliereService) {
    this.filiereService
      .getFilieres()
      .subscribe( (filieres : Filiere[]) => this.filieres = filieres);
  }

  ngOnInit(): void {
  }



}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleByIdComponent } from './module-by-id.component';

describe('ModuleByIdComponent', () => {
  let component: ModuleByIdComponent;
  let fixture: ComponentFixture<ModuleByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModuleByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

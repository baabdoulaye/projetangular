import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'module-by-id',
  templateUrl: './module-by-id.component.html',
  styleUrls: ['./module-by-id.component.css']
})
export class ModuleByIdComponent implements OnInit {

  
  urlParam: string | null = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg');
  }

}

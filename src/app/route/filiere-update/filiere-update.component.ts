import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Filiere } from 'src/app/interfaces/filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'filiere-update',
  templateUrl: './filiere-update.component.html',
  styleUrls: ['./filiere-update.component.css']
})
export class FiliereUpdateComponent implements OnInit {

  urlParam : string | null = null;
  filiere : Filiere = {libelle:''};

  constructor(private route : ActivatedRoute, private filiereService : FiliereService) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg');

    if (typeof(this.urlParam) === 'string'){
      const id = parseInt(this.urlParam);
      this.filiereService
        .getFiliere(id)
        .subscribe( (filiere : Filiere) => this.filiere = filiere)
      ;
    }
  }

  handleClick(){}
}

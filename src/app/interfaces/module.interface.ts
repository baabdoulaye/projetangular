import { Filiere } from "./filiere.interface";
import { Personne } from "./personne.interface";

export interface Module {
    id ?: number,
    libelle : string,
    dateDebut : Date,
    dateFin : Date,
    formateur ?: Personne,
    filiere ?: Filiere
}

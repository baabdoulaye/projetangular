import { Module } from "./module.interface";
import { Filiere } from "./filiere.interface";

export interface Personne {
    id ?: number,
    nom : string,
    prenom : string,
    type : string,
    filiere ?: Filiere,
    modules ?: Module[]
}